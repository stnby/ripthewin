#!/bin/bash

msg() {
  echo -e "\e[1;32m[+]\e[0m $1"
}

found() {
  echo -e "\e[1;35m[+]\e[0m $1"
}

err() {
  echo -e "\e[1;31m[!]\e[0m $1"
}

warn() {
  echo -e "\e[1;33m[-]\e[0m $1"
}

stuff() {
  dir="/mnt$1"
  if mkdir -p $dir ; then
    if mount $1 $dir ; then
      msg "Succesfully mounted $1 to $dir"
      samfile="$dir/Windows/System32/config/SAM"
      if [ -f $samfile ] ; then
        found "Found SAM file in $1"
        echo -e "1\n2\nq\ny\n" | chntpw $samfile >/dev/null
        found "Succesfully enabled Administrator account"
      else
        warn "Couldn't find SAM file in $1"
      fi
      umount $dir
      rmdir --ignore-fail-on-non-empty -p $dir
    else
      err "Couldn't mount $1"
      rmdir --ignore-fail-on-non-empty -p $dir
    fi
  else
    err "Couldn't create dir in $dir"
  fi
}

main() {
  partitions=$(fdisk -l | grep "Microsoft basic data" | awk 'BEGIN { ORS=" " }; {print $1}')
  for part in $partitions ; do
    stuff $part
  done
}

main
